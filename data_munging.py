import re

class Munging:

    # def __init__(self):
    #     raise NotImplementedError

    def load_data(self, file_name):
        self.data = []
        first_line = True
        with open(file_name) as file:
            for line in file:
                if first_line:
                    first_line = False
                else:
                    line = line.strip()
                    line = re.sub(r'[\s]+',',',line)
                    line = re.sub(r'[*]+','',line)
                    line = line.split(',')
                    self.data.append(line)


    def solution(self,file_name, o_index, f_index,s_index):
        self.load_data(file_name)
        res={}
        for i in range(len(self.data)):
            if len(self.data[i])>1:
                x = abs(float(self.data[i][f_index])- float(self.data[i][s_index]))
                res[self.data[i][o_index]] = int(x)
                sorted_x = sorted(res.items(), key=lambda kv: kv[1])
        return sorted_x[0][0]

